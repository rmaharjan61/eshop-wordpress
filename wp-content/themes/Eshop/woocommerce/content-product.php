<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

// Ensure visibility.
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}
?>
	<div class="row">
		<div class="col-6 col-sm-4 col-md-3">
<?php
if ( is_shop() || is_archive() ):
	?>
<?php
endif;
?>
		<div class="product-default inner-quickview inner-icon">
			<figure>
				<a href="<?= $product->get_permalink() ?>">
					<img src="<?php woocommerce_template_loop_product_thumbnail(); ?>" style="height: 190px; width: 190px; object-fit: cover;">
						</a>
						<div class="label-group">
							<div class="product-label label-hot">HOT</div>
							<div class="product-label label-sale">-20%</div>
						</div>
						<div class="btn-icon-group">
							<button class="btn-icon btn-add-cart" data-toggle="modal" data-target="#addCartModal"><i class="icon-shopping-cart"></i></button>
						</div>
						<a href="ajax/product-quick-view.html" class="btn-quickview" title="Quick View">Quick View</a> 
					</figure>
					<div class="product-details">
						<div class="category-wrap">
							<div class="category-list">
								<a href="category.html" class="product-category">category</a>
							</div>
							<a href="#" class="btn-icon-wish"><i class="icon-heart"></i></a>
						</div>
						<h2 class="product-title">
							<a href="product-single.html">Fleece Jacket</a>
						</h2>

						<div class="price-box">
							<span class="old-price">$90.00</span>
							<span class="product-price">$70.00</span>
						</div><!-- End .price-box -->
					</div><!-- End .product-details -->
				</div>

<?php
if ( is_shop() || is_archive() ):
	?>
			</div><!-- End .col-md-3 -->
				</div>
<?php
endif;


