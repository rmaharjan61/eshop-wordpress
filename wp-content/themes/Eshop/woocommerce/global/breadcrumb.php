<?php
/**
 * Shop breadcrumb
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/global/breadcrumb.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @package 	WooCommerce/Templates
 * @version     2.3.0
 * @see         woocommerce_breadcrumb()
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}



if ( ! empty( $breadcrumb ) ) {
	echo $wrap_before;
?>
	<div class="container">
				<nav aria-label="breadcrumb" class="breadcrumb-nav">
					<ol class="breadcrumb">
						

<?php
	foreach ( $breadcrumb as $key => $crumb ) {  //print_r($crumb[0]); ?>

		<li class="breadcrumb-item"><a href="<?php echo esc_url($crumb) ?>">
			<?php if (esc_html($crumb[0]) === "Home") { 
				?>
				<i class="icon-home"></i>
			<?php } 

			else{
				echo esc_html($crumb[0]);
			}
			?>
			
		

		</a></li>

		<?php
		// var_dump($crumb[1]);
		// echo $before;

		// if ( ! empty( $crumb[1] ) && sizeof( $breadcrumb ) !== $key + 1 ) {
		// 	echo '<li class="breadcrumb-item"> <a href="' . esc_url( $crumb[0] ) . '">' . esc_html( $crumb[1] ) . '</a> </li>';
		// } 
		// else {
		// 	echo esc_html( $crumb[0] );
		// }

		// echo $after;

		// if ( sizeof( $breadcrumb ) !== $key + 1 ) {
		// 	echo $delimiter;
		// }

	}
	?>
					</ol>
				</nav>
	<?php 
	echo $wrap_after;

}
