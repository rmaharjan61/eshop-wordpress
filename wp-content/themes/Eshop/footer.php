		</main><!-- End .main -->

<?php $contact = get_field('contact', 'option'); ?>
<?php $social = get_field('social', 'option'); ?>


<footer class="footer bg-dark">
			<div class="footer-middle">
				<div class="container">
					<div class="row">
						<div class="col-lg-3 col-sm-6">
							<div class="widget">
								<h4 class="widget-title">Contact Info</h4>
								<ul class="contact-info">
									<li>
										<span class="contact-info-label">Address</span><?= $contact['address'] ?>
									</li>
									<li>
										<span class="contact-info-label">Phone</span> <a href="tel:<?= $contact['contact_number'] ?>"><?= $contact['contact_number'] ?></a>
									</li>
									<li>
										<span class="contact-info-label">Email</span> <a href="mailto:<?= $contact['email'] ?>"><?= $contact['email'] ?></a>
									</li>
									<li>
										<span class="contact-info-label">Working Days/ Hours</span>
										<?= $contact['working_days'] ?> / <?= $contact['working_hours'] ?>
									</li>
								</ul>
								<div class="social-icons">
									<a href="<?= $social['instagram'] ?>" class="social-icon social-instagram icon-instagram" target="_blank" title="Instagram"></a>
									<a href="<?= $social['twitter'] ?>" class="social-icon social-twitter icon-twitter" target="_blank" title="Twitter"></a>
									<a href="<?= $social['facebook'] ?>" class="social-icon social-facebook icon-facebook" target="_blank" title="Facebook"></a>
								</div><!-- End .social-icons -->
							</div><!-- End .widget -->
						</div><!-- End .col-lg-3 -->

<!-- 						<div class="col-lg-3 col-sm-6">
							<div class="widget">
								<h4 class="widget-title">Customer Service</h4>

								<ul class="links">
									<li><a href="#">Pages</a></li>
									<li><a href="#">Blogs</a></li>
									<li><a href="#">Contacts</a></li>
									<li><a href="#">Help & FAQs</a></li>
									<li><a href="#">Order Tracking</a></li>
								</ul>
							</div>
						</div> -->
						<!-- End .col-lg-3 -->

					<!-- 	<div class="col-lg-3 col-sm-6">
							<div class="widget">
								<h4 class="widget-title">Popular Tags</h4>

								<div class="tagcloud">
									<a href="#">Bag</a>
									<a href="#">Black</a>
									<a href="#">Blue</a>
									<a href="#">Clothes</a>
									<a href="#">Hub</a>
									<a href="#">Shirt</a>
									<a href="#">Shoes</a>
									<a href="#">Skirt</a>
									<a href="#">Sports</a>
									<a href="#">Sweater</a>
								</div>
							</div>
						</div> -->
                       <!-- End .col-lg-3 -->

						<div class="col-lg-3 col-sm-6">
							<h4 class="widget-title">Payments Methods</h4>
								<div class="d-flex justify-content-start align-items-center flex-wrap">
							 	<?php 
					          	$images = $contact['payments_methods'];
					         		foreach ($images as $img) {
									?>      
		                   				<img src="<?php echo $img; ?>" alt="payment methods" class="footer-payments py-3 " style="width: 100px; height: 50px; object-fit: cover;">
		           				 <?php  
		           					}  
		            			?>
            			</div>
					<!-- End .col-lg-3 -->



					</div><!-- End .row -->
				</div><!-- End .container -->
			</div><!-- End .footer-middle -->

			<div class="container">
				<div class="footer-bottom d-flex justify-content-center align-items-center flex-wrap">
					<p class="footer-copyright py-3 pr-4 mb-0">&copy; 2021 Porto Eshop . All Rights Reserved. | Website by <a href="https://www.nirvan.studio"> Nirvan Studio. </a></p>

				</div><!-- End .footer-bottom -->
			</div><!-- End .container -->
		</footer><!-- End .footer -->
	</div><!-- End .page-wrapper -->

	<div class="mobile-menu-overlay"></div><!-- End .mobil-menu-overlay -->

	<div class="mobile-menu-container">
		<div class="mobile-menu-wrapper">
			<span class="mobile-menu-close"><i class="icon-cancel"></i></span>
			<nav class="mobile-nav">
				<ul class="mobile-menu mb-3">
					<li class="active"><a href="index.html">Home</a></li>
					<li>
						<a href="category.html">Categories</a>
						<ul>
							<li><a href="category-banner-full-width.html">Full Width Banner</a></li>
							<li><a href="category-banner-boxed-slider.html">Boxed Slider Banner</a></li>
							<li><a href="category-banner-boxed-image.html">Boxed Image Banner</a></li>
							<li><a href="category-sidebar-left.html">Left Sidebar</a></li>
							<li><a href="category-sidebar-right.html">Right Sidebar</a></li>
							<li><a href="category-flex-grid.html">Product Flex Grid</a></li>
							<li><a href="category-horizontal-filter1.html">Horizontal Filter 1</a></li>
							<li><a href="category-horizontal-filter2.html">Horizontal Filter 2</a></li>
							<li><a href="#">List Types</a></li>
							<li><a href="category-infinite-scroll.html">Ajax Infinite Scroll<span class="tip tip-new">New</span></a></li>
							<li><a href="category.html">3 Columns Products</a></li>
							<li><a href="category-4col.html">4 Columns Products</a></li>
							<li><a href="category-5col.html">5 Columns Products</a></li>
							<li><a href="category-6col.html">6 Columns Products</a></li>
							<li><a href="category-7col.html">7 Columns Products</a></li>
							<li><a href="category-8col.html">8 Columns Products</a></li>
						</ul>
					</li>
					<li>
						<a href="product-single.html">Products</a>
						<ul>
							<li>
								<a href="#">Variations</a>
								<ul>
									<li><a href="product-single.html">Horizontal Thumbs</a></li>
									<li><a href="product-full-width.html">Vertical Thumbnails<span class="tip tip-hot">Hot!</span></a></li>
									<li><a href="product-single.html">Inner Zoom</a></li>
									<li><a href="product-addcart-sticky.html">Addtocart Sticky</a></li>
									<li><a href="product-sidebar-left.html">Accordion Tabs</a></li>
								</ul>
							</li>
							<li>
								<a href="#">Variations</a>
								<ul>
									<li><a href="product-sticky-tab.html">Sticky Tabs</a></li>
									<li><a href="product-simple.html">Simple Product</a></li>
									<li><a href="product-sidebar-left.html">With Left Sidebar</a></li>
								</ul>
							</li>
							<li>
								<a href="#">Product Layout Types</a>
								<ul>
									<li><a href="product-single.html">Default Layout</a></li>
									<li><a href="product-extended-layout.html">Extended Layout</a></li>
									<li><a href="product-full-width.html">Full Width Layout</a></li>
									<li><a href="product-grid-layout.html">Grid Images Layout</a></li>
									<li><a href="product-sticky-both.html">Sticky Both Side Info<span class="tip tip-hot">Hot!</span></a></li>
									<li><a href="product-sticky-info.html">Sticky Right Side Info</a></li>
								</ul>
							</li>
						</ul>
					</li>
					<li>
						<a href="#">Pages<span class="tip tip-hot">Hot!</span></a>
						<ul>
							<li><a href="cart.html">Shopping Cart</a></li>
							<li>
								<a href="#">Checkout</a>
								<ul>
									<li><a href="checkout-shipping.html">Checkout Shipping</a></li>
									<li><a href="checkout-shipping-2.html">Checkout Shipping 2</a></li>
									<li><a href="checkout-review.html">Checkout Review</a></li>
								</ul>
							</li>
							<li><a href="about.html">About</a></li>
							<li><a href="#" class="login-link">Login</a></li>
							<li><a href="forgot-password.html">Forgot Password</a></li>
						</ul>
					</li>
					<li><a href="blog.html">Blog</a>
						<ul>
							<li><a href="single.html">Blog Post</a></li>
						</ul>
					</li>
					<li><a href="contact.html">Contact Us</a></li>
					<li><a href="#">Special Offer!<span class="tip tip-hot">Hot!</span></a></li>
					<li><a href="https://1.envato.market/DdLk5" target="_blank">Buy Porto!<span class="tip tip-new">New</span></a></li>
				</ul>

				<ul class="mobile-menu">
					<li><a href="my-account.html">Track Order </a></li>
					<li><a href="about.html">About</a></li>
					<li><a href="category.html">Our Stores</a></li>
					<li><a href="blog.html">Blog</a></li>
					<li><a href="contact.html">Contact</a></li>
					<li><a href="#">Help &amp; FAQs</a></li>
				</ul>
			</nav><!-- End .mobile-nav -->

			<div class="social-icons">
				<a href="#" class="social-icon" target="_blank"><i class="icon-facebook"></i></a>
				<a href="#" class="social-icon" target="_blank"><i class="icon-twitter"></i></a>
				<a href="#" class="social-icon" target="_blank"><i class="icon-instagram"></i></a>
			</div><!-- End .social-icons -->
		</div><!-- End .mobile-menu-wrapper -->
	</div><!-- End .mobile-menu-container -->


	<!-- Add Cart Modal -->
	<div class="modal fade" id="addCartModal" tabindex="-1" role="dialog" aria-labelledby="addCartModal" aria-hidden="true">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-body add-cart-box text-center">
			<p>You've just added this product to the<br>cart:</p>
			<h4 id="productTitle"></h4>
			<img src="#" id="productImage" width="100" height="100" alt="adding cart image">
			<div class="btn-actions">
				<a href="cart.html"><button class="btn-primary">Go to cart page</button></a>
				<a href="#"><button class="btn-primary" data-dismiss="modal">Continue</button></a>
			</div>
		  </div>
		</div>
	  </div>
	</div>

	<a id="scroll-top" href="#top" title="Top" role="button"><i class="icon-angle-up"></i></a>
  
    <?php
    	wp_footer();
    ?>
    
</body>
</html> 