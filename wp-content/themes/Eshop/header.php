<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
     
    <!-- Favicon -->
    <link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri();?>/assets/images/icons/favicon.ico"/>
    
	<?php
		wp_head();
	?>

	</head>

	<body>
	
		<div class="page-wrapper">
			<?php get_template_part('template-parts/header/top-header'); ?>
			<?php get_template_part('template-parts/header/middle-header'); ?>
			<?php get_template_part( 'template-parts/header/navigation' ); ?>
		</div>


	<main class="main">
