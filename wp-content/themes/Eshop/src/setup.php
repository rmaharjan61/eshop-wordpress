<?php 

function eshop_theme_support(){
	//dynamic title support
	add_theme_support('title-tag');
	add_theme_support('custom-logo');
	add_theme_support('post-thumbnails');
    add_theme_support('woocommerce');
}
add_action('after_setup_theme','eshop_theme_support');


function register_menus(){
	register_nav_menus([
		'primary' => 'Primary Menu',
		'footer' => 'Footer Menu'
	]);
}
add_action('init','register_menus');



function eshop_theme_register_styles(){
    $version = wp_get_theme()->get('Version');

    wp_enqueue_style('eshop-theme-bootstrap', get_template_directory_uri() . '/assets/css/bootstrap.min.css', array(), '$version', 'all' );


    wp_enqueue_style('eshop-theme-style-min', get_template_directory_uri() . '/assets/css/style.min.css', array('eshop-theme-bootstrap'), '$version', 'all' );

    wp_enqueue_style('eshop-theme-fontawesome', get_template_directory_uri() . '/assets/vendor/fontawesome-free/css/all.min.css', array(), '$version', 'all' );
   
   wp_enqueue_style('school-theme-google-fonts-opensans', "https://fonts.googleapis.com/css?family=Open+Sans:Open+Sans:300,400,600,700,800", array(), '$version', 'all' );

   wp_enqueue_style('school-theme-google-fonts-poppins', "https://fonts.googleapis.com/css?family=Open+Sans:Poppins:300,400,600,700,800", array(), '$version', 'all' );

}
add_action('wp_enqueue_scripts', 'eshop_theme_register_styles');




function eshop_theme_register_scripts(){
	$version = wp_get_theme()->get('Version');

    wp_enqueue_script('eshop-theme-jquery',   get_template_directory_uri() . '/assets/js/jquery.min.js', array(), '$version', true);

    wp_enqueue_script('eshop-theme-bootstrap-js',   get_template_directory_uri() . '/assets/js/bootstrap.bundle.min.js', array(), '$version', true);

    wp_enqueue_script('eshop-theme-plugins',   get_template_directory_uri() . '/assets/js/plugins.min.js', array(), '$version', true);

    wp_enqueue_script('eshop-theme-isotope',   get_template_directory_uri() . '/assets/js/optional/isotope.pkgd.min.js', array(), '$version', true);

    wp_enqueue_script('eshop-theme-js-main',   get_template_directory_uri() . '/assets/js/main.min.js', array(), '$version', true);

    wp_enqueue_script( 'woocommerce-custom-script', get_template_directory_uri() . '/assets/js/woocommerce-custom.js', [], '1.0', true );


    wp_localize_script( 'woocommerce-custom-script', 'es',
        [
            'ajax_url' => admin_url( 'admin-ajax.php' )
        ] );

}
add_action('wp_enqueue_scripts', 'eshop_theme_register_scripts');

