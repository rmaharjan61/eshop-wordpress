<?php

function get_product_quick_view_html($product_id, $variation = [])
{
    $product = wc_get_product($product_id);
    $parent_product = '';
    if ($product->get_parent_id()) {
        $parent_product = wc_get_product($product->get_parent_id());
    }
    ?>
    <div class="row">
        <div class="col-md-6 product_img">
            <div class="owl-carousel owl-theme" id="b-product_pop_slider">
                <?php

                $image_ids = $product->get_gallery_image_ids();

                if (empty($image_ids)) {
                    $image_ids = [$product->get_image_id()];

                } else {
                    array_unshift($image_ids, $product->get_image_id());
                }
                if ($image_ids):
                    foreach ($image_ids as $image_id):
                        ?>
                        <div>
                            <?php
                            $image = wp_get_attachment_image_url($image_id, 'full');
                            $image = getResizedImage($image, [475, 600]);
                            echo \NextGenImage\getWebPHTML($image['webp'], $image['orig'], [
                                'class' => 'img-fluid d-block m-auto',
                                'alt' => esc_attr($product->get_name())
                            ]);
                            ?>
                        </div>
                    <?php
                    endforeach;
                endif;
                ?>
            </div>
        </div>
        <div class="col-md-6 product_content pr-5 pt-4">
            <div class="b-product_single_summary">
                <h1><?= $product->get_name() ?></h1>
                <p class="b-price">
                    <span class="b-amount"><?= $product->get_price_html() ?></span>
                </p>
                <div class="b-produt_description">
                    <p><?= $product->get_short_description() ?></p>
                </div>
                <div class="b-product_attr">
                    <?php
                    $attributes = array_filter(($parent_product) ? $parent_product->get_attributes() : $product->get_attributes(), 'wc_attributes_array_filter_visible');


                    foreach ($attributes as $attribute) {
                        $values = array();

                        if ($attribute->is_taxonomy()) {
                            $attribute_taxonomy = $attribute->get_taxonomy_object();
                            $attribute_values = wc_get_product_terms(($parent_product) ? $parent_product->get_id() : $product->get_id(), $attribute->get_name(), array('fields' => 'all'));

                            foreach ($attribute_values as $attribute_value) {
                                $value_slug = esc_attr($attribute_value->slug);
                                $value_id = esc_attr($attribute_value->term_id);
                                $value_name = esc_html($attribute_value->name);

                                if ($attribute_taxonomy->attribute_public) {
                                    $values[] = '<a href="' . esc_url(get_term_link($attribute_value->term_id, $attribute->get_name())) . '" rel="tag">' . $value_name . '</a>';
                                } else {
                                    $values[] = [$value_slug, $value_name, $value_id];
                                }
                            }
                        } else {
                            $values = $attribute->get_options();

                            foreach ($values as &$value) {
                                $value = make_clickable(esc_html($value));
                            }
                        }


                        $product_attributes['attribute_' . sanitize_title_with_dashes($attribute->get_name())] = array(
                            'label' => [
                                'attribute_' . $attribute->get_name(),
                                wc_attribute_label($attribute->get_name())
                            ],
                            'value' => apply_filters('woocommerce_attribute', $values, $attribute, $values),
                        );
                    }

                    foreach ($product_attributes as $product_attribute_key => $product_attribute):
                        ?>
                        <div class="b-product_attr_single">
                            <label for="<?= $product_attribute_key ?>"><?= $product_attribute['label'][1] ?>
                                :</label>
                            <select name="<?= $product_attribute_key ?>"
                                    id="<?= $product_attribute_key ?>"
                                    class="variation-select">
                                <?php
                                if (count($product_attribute['value']) > 1):
                                    ?>
                                    <option value="">Choose an option</option>
                                <?php
                                endif;
                                foreach ($product_attribute['value'] as $option):

                                    if (is_array($option)):
                                        ?>
                                        <option
                                                value="<?= $option[0] ?>"
                                                <?= ($variation[$product_attribute_key] == $option[0]) ? 'selected' : '' ?>><?= $option[1] ?></option>
                                    <?php
                                    else:
                                        ?>
                                        <option
                                                value="<?= $option ?>"
                                                <?= ($variation[$product_attribute_key] == $option) ? 'selected' : '' ?>><?= $option ?></option>
                                    <?php
                                    endif;
                                endforeach;
                                ?>
                            </select>
                        </div>
                    <?php
                    endforeach;
                    ?>
                </div>
                <div class="b-product_single_action clearfix <?= (!$product->is_in_stock()) ? "d-none" : "" ?>">
                    <p class="stock in-stock"><?= $product->get_stock_quantity() ?> in stock</p>
                    <div class="clearfix">
                        <div class="b-quantity pull-left">
                            <input type="button" value="-" class="b-minus">
                            <input type="text" step="1" min="<?= $product->get_min_purchase_quantity() ?>"
                                   max="<?= ($product->get_max_purchase_quantity() != -1) ? $product->get_max_purchase_quantity() : '' ?>"
                                   name="b-quantity" value="1" title="Qty"
                                   class="input-text qty text" size="4" pattern="[0-9]*" inputmode="numeric">
                            <input type="button" value="+" class="b-plus">
                        </div>
                        <button
                                class="text-uppercase pull-left btn quick-add-to-cart"
                                data-product_id="<?= ($parent_product) ? $parent_product->get_id() : $product->get_id() ?>"
                                data-product-type="<?= $product->get_type() ?>">Add to cart
                        </button>
                    </div>
                </div>
                <?php
                if (!$product->is_in_stock()):
                    ?>
                    <div>
                        <p class="stock out-of-stock">Out of Stock</p>
                    </div>
                <?php
                endif;
                ?>
                <div class="b-product_single_option">
                    <ul class="pl-0 list-unstyled">
                        <li><b class="text-uppercase">Sku</b>: <?= $product->get_sku() ?></li>
                        <li><b>Category</b>:
                            <?php
                            $categories = wp_get_post_terms(($parent_product) ? $parent_product->get_id() : $product->get_id(), 'product_cat');

                            foreach ($categories as $category):
                                ?>
                                <a href="<?= get_category_link($category->term_id) ?>"><?= $category->name ?></a>
                            <?php
                            endforeach;
                            ?>
                        </li>
                        <li><?php ecommerce_product_sharing($product->get_id()); ?></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <?php
}

function get_single_product_html($product_id)
{
    $product = wc_get_product($product_id);
    // var_dump($product);
    ?>

                            <div class="product-default">
  
                                <figure>
                                    <a href="<?= $product->get_permalink() ?>">
                                        <img src="<?php echo wp_get_attachment_image_url($product->get_image_id()); ?>"  >
                                    </a>
<!--                                     <div class="label-group">
                                        <div class="product-label label-hot">HOT</div>
                                        <div class="product-label label-sale">20% Off</div>
                                    </div> -->
                                </figure>
                                <div class="product-details">
                                    <div class="category-list">
                                        <?php 
                                            $id = $product->get_id();
                                            if( $term = get_the_terms( $id, 'product_cat' ) ){
                                                // var_dump($term[0]->slug);
                                                // print_r($term);
                                            }

                                            ?>
                                        <a href="<?php echo get_term_link($term[0]->slug, 'product_cat'); ?>" class="product-category"><?php echo $term[0]->name;  ?></a>
                                    </div>
                                    <h3 class="product-title">
                                        <a href="<?= $product->get_permalink() ?>"><?= $product->get_name() ?></a>
                                        <!-- <a href="product-single.html">Product Short Name</a> -->
                                    </h3>
                                    <div class="ratings-container">
                                        
                                    </div><!-- End .product-container -->
                                    <div class="price-box">
                                        <?php 
                                            $s_price = $product->get_sale_price();
                                            if (!empty($s_price)) { ?>

                                        <del class="old-price">Rs. <?php echo $product->get_regular_price(); ?></del>
                                        <span class="product-price">Rs. <?php echo $product->get_sale_price(); ?></span>        
                                           
                                          <?php  }
                                          else{ ?>
                                            <span class="product-price">Rs. <?php echo $product->get_regular_price(); ?></span> 
                                          <?php  }
                                         ?>
                                        
                                    </div><!-- End .price-box -->
                                    <div class="product-action">
                                        <a href="#" class="btn-icon-wish"><i class="icon-heart"></i></a>


                <button class="btn-icon btn-add-cart" 
                   data-product_id="<?= $product->get_id() ?>">ADD TO CART </button>


                                       


                                        <a href="ajax/product-quick-view.html" class="btn-quickview" title="Quick View"><i class="fas fa-external-link-alt"></i></a>
                                    </div>
                                </div><!-- End .product-details -->
                            </div>



    <?php
}