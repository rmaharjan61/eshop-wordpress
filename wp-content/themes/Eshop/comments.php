 <div class="mu-comments-area">
                      <h3>5 Comments</h3>
                      <div class="comments">

                        <div class="comments-headers">
                          <h2 class="reply-title">
                            <?php 
                              if(!have_comments() ){
                                "Leave a comment"
                              }
                              else{
                               echo get_comments_number(). "comment";
                              }
                             ?>
                          </h2>
                        </div> <!-- end of comment headers -->

                        <div class="comments-inner">
                          <?php 
                            wp_list_comments(
                              array(                        
                              'avatar_size' => 120,
                              'style' => 'div'
                                )
                            );
                           ?>
                        </div> <!-- end of comment inner -->

                      </div> <!-- end of comments -->

                      <hr class="" aria-hidden="true">

                      <?php 
                         if (comments_open()) {
                            comment_form(
                              array(
                                  'class_form' => '',
                                  'title_reply_before' => '<h2 id="reply-tile" class="comment-reply-title" ',
                                  'title_reply_after' => '</h2>' 
                              )
                            );
                         }
                       ?>
                    </div>