 <!-- Start service  -->
<section id="mu-service">
<div class="container">
  <div class="row">
    <div class="col-lg-12 col-md-12">
      <div class="mu-service-area">
               <?php while (have_rows('block')):
                    the_row();

                    $icon = get_sub_field('icon');
                    $background_color = get_sub_field('background_color');
                    $heading = get_sub_field('heading');
                    $paragraph = get_sub_field('paragraph');
                    ?>

        <!-- Start single service -->
        <div class="mu-service-single" style="background-color: <?php echo $background_color; ?>; height: 240px; width: 350px;">
          <span class=""><?php echo $icon;  ?></span>
          <h3><?php echo $heading; ?></h3>
          <p><?php echo $paragraph; ?></p>
        </div>   
         <?php endwhile; ?>    
      </div>
    </div>
  </div>
</div>
</section>
<!-- End service  -->