<!-- Start our teacher -->
<section id="mu-our-teacher">
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="mu-our-teacher-area">
        <!-- begain title -->
        <div class="mu-title">
          <h2><?php echo the_sub_field('heading'); ?></h2>
          <p><?php echo the_sub_field('headline_paragraph'); ?></p>
        </div>
        <!-- end title -->
        <!-- begain our teacher content -->
        <div class="mu-our-teacher-content">
          <div class="row">

        <?php while (have_rows('teacher_details')):
                    the_row();

                    $image = get_sub_field('image');
                    $name = get_sub_field('name');
                    $post = get_sub_field('post');
                    $paragraph = get_sub_field('paragraph');
                    $facebook = get_sub_field('facebook');
                    $twitter = get_sub_field('twitter');
                    $linkedin = get_sub_field('linked_in');
                    $googleplus = get_sub_field('google_plus');
                    ?>
            <div class="col-lg-3 col-md-3  col-sm-6">
              <div class="mu-our-teacher-single">
                <figure class="mu-our-teacher-img">
                  <img src="<?php echo $image; ?>" alt="teacher img">
                  <div class="mu-our-teacher-social">
                    <a href="<?= $facebook; ?>"><span class="fa fa-facebook"></span></a>
                    <a href="<?= $twitter; ?>"><span class="fa fa-twitter"></span></a>
                    <a href="<?= $linkedin; ?>"><span class="fa fa-linkedin"></span></a>
                    <a href="<?= $googleplus; ?>"><span class="fa fa-google-plus"></span></a>
                  </div>
                </figure>                    
                <div class="mu-ourteacher-single-content">
                  <h4><?= $name; ?></h4>
                  <span><?= $post; ?></span>
                  <p><?= $paragraph; ?></p>
                </div>
              </div>
            </div>
             <?php endwhile; ?> 
          </div>
        </div> 
        <!-- End our teacher content -->           
      </div>
    </div>
  </div>
</div>
</section>
<!-- End our teacher