<!-- Start latest course section -->
<section id="mu-latest-courses">
<div class="container">
  <div class="row">
    <div class="col-lg-12 col-md-12">
      <div class="mu-latest-courses-area">
        <!-- Start Title -->
        <div class="mu-title">
          <h2><?php echo get_sub_field('heading'); ?></h2>
          <p><?php echo get_sub_field('headline_paragraph'); ?></p>
        </div>
        <!-- End Title -->
        <!-- Start latest course content -->
        <div id="mu-latest-course-slide" class="mu-latest-courses-content">
            <?php $condition = array(
                'post_type' => 'courses',
                'post_status' => 'publish',
                'posts_per_page' => '9'
            );
            $query = new WP_Query($condition);
            if ($query->have_posts()) {
                while ($query->have_posts()) :
                    $query->the_post();
                    $img = get_the_post_thumbnail();
                    ?>
              
          <div class="col-lg-4 col-md-4 col-xs-12">

              <div class="mu-latest-course-single">
              <figure class="mu-latest-course-img">
                <a href="<?php the_permalink(); ?>"><img src="<?php the_post_thumbnail_url(); ?>"></a>
                <figcaption class="mu-latest-course-imgcaption">
                  Teacher: <?php echo get_field('teacher'); ?>
                  <span><i class="fa fa-clock-o"></i><?php echo get_field('duration'); ?> Days</span>
                </figcaption>
              </figure>
              <div class="mu-latest-course-single-content">
                <h4><a href="<?php the_permalink(); ?>"><?php echo the_title(); ?></a></h4> 
                <p><?php echo get_field('short_description'); ?></p>
                <div class="mu-latest-course-single-contbottom">
                  <a class="mu-course-details" href="<?php the_permalink(); ?>">Details</a>
                  <span class="mu-course-price" > Rs. <?php echo get_field('price'); ?></span>
                </div>
              </div>
            </div>



          </div>
                <?php
                endwhile;
                wp_reset_postdata();
            }
            ?>

        </div>
        <!-- End latest course content -->
      </div>
    </div>
  </div>
</div>
</section>
<!-- End latest course section -->