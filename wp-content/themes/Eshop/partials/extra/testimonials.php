<!-- Start testimonial -->
<section id="mu-testimonial" style="background-image: linear-gradient(to bottom, rgba(0, 0, 0, 0) 0%, rgba(0, 0, 0, 1) 100%), url(<?php echo get_sub_field('background_img'); ?> );">
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="mu-testimonial-area">
        <div id="mu-testimonial-slide" class="mu-testimonial-content">
                    <?php while (have_rows('block')):
                    the_row();

                    $image = get_sub_field('image');
                    $name = get_sub_field('name');
                    $paragraph = get_sub_field('paragraph');
                    $post = get_sub_field('post');
                    ?>
          <!-- start testimonial single item -->
          <div class="mu-testimonial-item">
            <div class="mu-testimonial-quote">
              <blockquote>
                <p><?= $paragraph; ?></p>
              </blockquote>
            </div>
            <div class="mu-testimonial-img">
              <img src="<?= $image; ?>" alt="img">
            </div>
            <div class="mu-testimonial-info">
              <h4><?= $name; ?></h4>
              <span><?= $post; ?></span>
            </div>
          </div>
          <!-- end testimonial single item -->
         <?php endwhile; ?> 
        </div>
      </div>
    </div>
  </div>
</div>
</section>
<!-- End testimonial -->