<!-- Page breadcrumb -->
 <section id="mu-page-breadcrumb">
   <div class="container">
     <div class="row">
       <div class="col-md-12">
         <div class="mu-page-breadcrumb-area">
           <h2>Course</h2>
           <ol class="breadcrumb">
            <li><a href="<?= get_home_url(); ?>">Home</a></li>            
            <li class="active">Course</li>
          </ol>
         </div>
       </div>
     </div>
   </div>
 </section>
 <!-- End breadcrumb -->
 <section id="mu-course-content">
   <div class="container">
     <div class="row">
       <div class="col-md-12">
         <div class="mu-course-content-area">
            <div class="row">
              <div class="col-md-9">
                <!-- start course content container -->
                <div class="mu-course-container">
                  <div class="row">
                      <?php
                      
                       $condition = array(
                            'post_type' => 'courses',
                            'post_status' => 'publish',
                            'posts_per_page' => '6',
                            'paged' => $paged
                        );
                        $query = new WP_Query($condition);
                        if ($query->have_posts()) {
                            while ($query->have_posts()) :
                                $query->the_post();
                                $img = get_the_post_thumbnail();
                                ?>
                    <div class="col-md-6 col-sm-6">
                    <div class="mu-latest-course-single">
                      <figure class="mu-latest-course-img-archive" >
                        <a href="<?php the_permalink(); ?>"><img src="<?php the_post_thumbnail_url(); ?>"></a>
                        <figcaption class="mu-latest-course-imgcaption">
                           Teacher: <?php echo get_field('teacher'); ?>
                            <span><i class="fa fa-clock-o"></i><?php echo get_field('duration'); ?> Days</span>
                        </figcaption>
                      </figure>
                      <div class="mu-latest-course-single-content">
                        <h4><a href="<?php the_permalink(); ?>"><?php echo the_title(); ?></a></h4> 
                         <p><?php echo get_field('short_description'); ?></p>
                        <div class="mu-latest-course-single-contbottom">
                         <a class="mu-course-details" href="<?php the_permalink(); ?>">Details</a>
                          <span class="mu-course-price" > Rs. <?php echo get_field('price'); ?></span>
                        </div>
                      </div>
                    </div> 

                  </div>                
                          <?php
                endwhile;
                wp_reset_postdata();
            }
            ?>
           
                   </div>
                </div>
                 <?php 
                      global $wp_query;
                      $page = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
                      $count_pages = wp_count_posts('page');
                      $total= $count_pages->publish;
                      custom_pagination($page,$total); 

                ?>
                <!-- end course content container -->
               
              </div>
              <div class="col-md-3">
                <!-- start sidebar -->
                <aside class="mu-sidebar">
                  <?php
        $categories =get_terms( array(
    'taxonomy' => 'coursesarchive',
    'hide_empty' => false,
) );

foreach( $categories as $category ) {
 echo '<div class="col-md-4"><a href="' . get_category_link($category->term_id) . '">' . $category->name . '</a></div>';   
} 
?>
                  <!-- start single sidebar -->
                  <div class="mu-single-sidebar">
                    <h3>Categories</h3>
                    <ul class="mu-sidebar-catg">
                      <li><a href="#">Web Design</a></li>
                      <li><a href="">Web Development</a></li>
                      <li><a href="">Math</a></li>
                      <li><a href="">Physics</a></li>
                      <li><a href="">Camestry</a></li>
                      <li><a href="">English</a></li>
                    </ul>
                  </div>
                  <!-- end single sidebar -->
                  <!-- start single sidebar -->
                  <div class="mu-single-sidebar">
                    <h3>Subjects</h3>
                    <ul class="mu-sidebar-catg">
                      <li><a href="#">Web Design</a></li>
                      <li><a href="">Web Development</a></li>
                      <li><a href="">Math</a></li>
                      <li><a href="">Physics</a></li>
                      <li><a href="">Camestry</a></li>
                      <li><a href="">English</a></li>
                    </ul>
                  </div>
                  <!-- end single sidebar -->
                  <!-- start single sidebar -->
                  <div class="mu-single-sidebar">
                    <h3>Archives</h3>
                    <ul class="mu-sidebar-catg mu-sidebar-archives">
                      <li><a href="#">May <span>(25)</span></a></li>
                      <li><a href="">June <span>(35)</span></a></li>
                      <li><a href="">July <span>(20)</span></a></li>
                      <li><a href="">August <span>(125)</span></a></li>
                      <li><a href="">September <span>(45)</span></a></li>
                      <li><a href="">October <span>(85)</span></a></li>
                    </ul>
                  </div>
                  <!-- end single sidebar -->
                </aside>
                <!-- / end sidebar -->
             </div>
           </div>
         </div>
       </div>
     </div>
   </div>
 </section>