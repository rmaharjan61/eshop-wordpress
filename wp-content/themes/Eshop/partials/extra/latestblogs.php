<!-- Start from blog -->
<section id="mu-from-blog">
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="mu-from-blog-area">
        <!-- start title -->
        <div class="mu-title">
          <h2><?php echo get_sub_field('heading'); ?></h2>
          <p><?php echo get_sub_field('headline_paragraph'); ?></p>
        </div>
        <!-- end title -->  
        <!-- start from blog content   -->
        <div class="mu-from-blog-content">
          <div class="row">

            <?php $condition = array(
                'post_type' => 'blogs',
                'post_status' => 'publish',
                'posts_per_page' => '3'
            );
            $query = new WP_Query($condition);
            if ($query->have_posts()) {
                while ($query->have_posts()) :
                    $query->the_post();
                    $img = get_the_post_thumbnail();
                    ?>

            <div class="col-md-4 col-sm-4">
              <article class="mu-blog-single-item">
                <figure class="mu-blog-single-img">
                  <a href="<?php the_permalink(); ?>"><img src="<?php the_post_thumbnail_url(); ?>"></a>
                  <figcaption class="mu-blog-caption">
                    <h3><a href="<?php the_permalink(); ?>"><?php echo the_title(); ?></a></h3>
                  </figcaption>                      
                </figure>
                <div class="mu-blog-meta">
                  <a href="<?php the_permalink(); ?>"><?php echo get_field('author'); ?></a>
                  <span><i class="fa fa-calendar"></i></span>
                  <a href="<?php the_permalink(); ?>"><?php echo get_field('date'); ?></a>
   
                  
                </div>
                <div class="mu-blog-description">
                  <p><?php echo get_field('short_description'); ?></p>
                  <a class="mu-read-more-btn" href="<?php the_permalink(); ?>">Read More</a>
                </div>
              </article>
            </div>

            <?php
                endwhile;
                wp_reset_postdata();
            }
            ?>

          </div>
        </div>     
        <!-- end from blog content   -->   
      </div>
    </div>
  </div>
</div>
</section>
<!-- End from blog -->