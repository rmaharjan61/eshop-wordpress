<!-- Start about us counter -->
<section id="mu-abtus-counter" style="background-image: linear-gradient(to bottom, rgba(0, 0, 0, 0) 0%, rgba(0, 0, 0, 1) 100%), url(<?php echo get_sub_field('background_image'); ?> );">
  
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="mu-abtus-counter-area">
        <div class="row">
          <?php while (have_rows('block')):
                    the_row();

                    $icon = get_sub_field('icon');
                    $number = get_sub_field('number');
                    $title = get_sub_field('title');
                    ?>
          <!-- Start counter item -->
          <div class="col-lg-3 col-md-3 col-sm-3">
            <div class="mu-abtus-counter-single">
              <span> <?php echo $icon; ?> </span>
              <h4 class="counter"><?php echo $number; ?></h4>
              <p><?php echo $title; ?></p>
            </div>
          </div>
          <?php endwhile; ?> 
        </div>
      </div>
    </div>
  </div>
</div>
</section>
<!-- End about us counter -->