<!-- Start about us -->
<section id="mu-about-us">
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="mu-about-us-area">           
        <div class="row">
          <div class="col-lg-6 col-md-6">
            <div class="mu-about-us-left">
              <!-- Start Title -->
              <div class="mu-title">
                <h2><?php echo the_sub_field('title'); ?></h2>              
              </div>
              <!-- End Title -->
              <p><?php echo the_sub_field('paragraph'); ?></p>
            </div>
          </div>
          <div class="col-lg-6 col-md-6">
            <div class="mu-about-us-right">                            
            <a id="mu-abtus-video" href="<?= the_sub_field('video');?>" target="mutube-video">
              <img src="<?php echo get_sub_field('video_thumbnail'); ?>" alt="img">
            </a>                
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</section>
<!-- End about us -->