<!-- Page breadcrumb -->
 <section id="mu-page-breadcrumb">
   <div class="container">
     <div class="row">
       <div class="col-md-12">
         <div class="mu-page-breadcrumb-area">
           <h2>Gallery</h2>
           <ol class="breadcrumb">
            <li><a href="<?= get_home_url(); ?>">Home</a></li>            
            <li class="active">Gallery</li>
          </ol>
         </div>
       </div>
     </div>
   </div>
 </section>
 <!-- End breadcrumb -->
 <!-- Start gallery  -->
 <section id="mu-gallery">
   <div class="container">
     <div class="row">
       <div class="col-md-12">
         <div class="mu-gallery-area">
          <!-- start title -->
          <div class="mu-title">
            <h2><?php echo get_sub_field('heading'); ?></h2>
            <p><?php echo get_sub_field('headline_paragraph'); ?></p>
          </div>
          <!-- end title -->

        <!-- start gallery content -->
          <div class="mu-gallery-content">
            <div class="mu-gallery-body">
              <ul id="mixit-container" class="row">
                       <?php while (have_rows('gallery_category')):
                            the_row();
                            $title = get_sub_field('title');
                        ?>
              <ul id="mixit-container" class="row">
                <!-- start single gallery image -->

                        <h2 style="padding-top: 30px; padding-bottom:30px; text-align: center;"><?= $title; ?></h2>
                        <?php 
                      $images = get_sub_field('images');
                        if( $images ): ?>
                              <?php foreach( $images as $image_id ): ?>

                <li class="col-md-4 col-sm-6 col-xs-12 mix lab">
                  <div class="mu-single-gallery">                  
                    <div class="mu-single-gallery-item">
                      <div class="mu-single-gallery-img">
                        <?php echo wp_get_attachment_image( $image_id); ?>
                      </div>
                      <div class="mu-single-gallery-info">
                        <div class="mu-single-gallery-info-inner">
                          <p><?php echo $title; ?></p>
                          <!-- <a href="http://localhost:8080/school_theme/wp-content/uploads/2021/05/image_2021-05-11_113029-300x150.png" data-fancybox-group="gallery" class="fancybox"><span class="fa fa-eye"></span></a> -->
                        </div>
                      </div>                  
                    </div>
                  </div>
                </li>
                <?php endforeach; ?>
                      <?php endif; ?>

          <!-- end gallery content -->
        </ul>
          <?php endwhile; ?> 
         </div>
       </div>
     </div>
   </div>
 </section>
 <!-- End gallery  -->