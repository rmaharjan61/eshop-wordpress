<!-- Page breadcrumb -->
 <section id="mu-page-breadcrumb">
   <div class="container">
     <div class="row">
       <div class="col-md-12">
         <div class="mu-page-breadcrumb-area">
           <h2>Blog Archive</h2>
           <ol class="breadcrumb">
            <li><a href="<?= get_home_url(); ?>">Home</a></li>            
            <li class="active">Blog Archive</li>
          </ol>
         </div>
       </div>
     </div>
   </div>
 </section>
 <!-- End breadcrumb -->
 <section id="mu-course-content">
   <div class="container">
     <div class="row">
       <div class="col-md-12">
         <div class="mu-course-content-area">
            <div class="row">
              <div class="col-md-9">
                <!-- start course content container -->
                <div class="mu-course-container mu-blog-archive">
                  <div class="row">
                <?php $condition = array(
                'post_type' => 'blogs',
                'post_status' => 'publish',
                'posts_per_page' => '6',
                'paged' => $paged
            );
            $query = new WP_Query($condition);
            if ($query->have_posts()) {
                while ($query->have_posts()) :
                    $query->the_post();
                    $img = get_the_post_thumbnail();
                    ?>
                    <div class="col-md-6 col-sm-6">
                      <article class="mu-blog-single-item">
                        <figure class="mu-blog-single-img">
                          <a href="<?php the_permalink(); ?>"><img src="<?php the_post_thumbnail_url(); ?>"></a>
                          <figcaption class="mu-blog-caption">
                           <h3><a href="<?php the_permalink(); ?>"><?php echo the_title(); ?></a></h3>
                          </figcaption>                      
                        </figure>
                        <div class="mu-blog-meta">
                          <a href="<?php the_permalink(); ?>"><?php echo get_field('author'); ?></a>
                          <span><i class="fa fa-calendar"></i></span>
                          <a href="<?php the_permalink(); ?>"><?php echo get_field('date'); ?></a>
                          </div>
                        <div class="mu-blog-description">
                          <p><?php echo get_field('short_description'); ?></p>
                          <a class="mu-read-more-btn" href="<?php the_permalink(); ?>">Read More</a>
                        </div>
                      </article> 
                    </div>               
               <?php
                endwhile;
                wp_reset_postdata();
            }
            ?>
                     </div>
                </div>
              
                <!-- end course content container -->
                   <?php 
                      global $wp_query;
                      $page = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
                      $count_pages = wp_count_posts('page');
                      $total= $count_pages->publish;
                      custom_pagination($page,$total); ?>
              </div>
              <div class="col-md-3">
                <!-- start sidebar -->
                <aside class="mu-sidebar">
                  <!-- start single sidebar -->
                  <div class="mu-single-sidebar">
                    <h3>Categories</h3>
                    <ul class="mu-sidebar-catg">
                      <li><a href="#">Web Design</a></li>
                      <li><a href="">Web Development</a></li>
                      <li><a href="">Math</a></li>
                      <li><a href="">Physics</a></li>
                      <li><a href="">Camestry</a></li>
                      <li><a href="">English</a></li>
                    </ul>
                  </div>
                  <!-- end single sidebar -->
                
                  <!-- start single sidebar -->
                  <div class="mu-single-sidebar">
                    <h3>Archives</h3>
                    <ul class="mu-sidebar-catg mu-sidebar-archives">
                      <li><a href="#">May <span>(25)</span></a></li>
                      <li><a href="">June <span>(35)</span></a></li>
                      <li><a href="">July <span>(20)</span></a></li>
                      <li><a href="">August <span>(125)</span></a></li>
                      <li><a href="">September <span>(45)</span></a></li>
                      <li><a href="">October <span>(85)</span></a></li>
                    </ul>
                  </div>
                  <!-- end single sidebar -->
              
                </aside>
                <!-- / end sidebar -->
             </div>
           </div>
         </div>
       </div>
     </div>
   </div>
 </section>
