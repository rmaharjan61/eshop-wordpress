<!-- Start features section -->
<section id="mu-features">
<div class="container">
  <div class="row">
    <div class="col-lg-12 col-md-12">
      <div class="mu-features-area">
        <!-- Start Title -->
        <div class="mu-title">
          <h2><?php echo the_sub_field('heading'); ?></h2>
          <p><?php echo the_sub_field('headline_paragraph'); ?></p>
        </div>
        <!-- End Title -->
        <!-- Start features content -->
        <div class="mu-features-content">
          <div class="row">
          <?php while (have_rows('features')):
                    the_row();

                    $icon = get_sub_field('icon');
                    $title = get_sub_field('title');
                    $paragraph = get_sub_field('paragraph');
                    $feature_link = get_sub_field('feature_link');
                    ?>
            <div class="col-lg-4 col-md-4  col-sm-6">
              <div class="mu-single-feature">
                <span> <?php echo $icon; ?>  </span>
                <h4><?php echo $title; ?></h4>
                <p><?php echo $paragraph; ?></p>
                <a href="<?= $feature_link; ?>">Read More</a>
              </div>
            </div>
            <?php endwhile; ?> 
          </div>
        </div>
        <!-- End features content -->
      </div>
    </div>
  </div>
</div>
</section>
<!-- End features section -->