
<section class="promo-section bg-dark" data-parallax="{'speed': 1.8, 'enableOnMobile': true}" style="background: <?php echo get_sub_field('color'); ?> center/cover url('<?php echo get_sub_field('image'); ?>');">
	<div class="promo-banner banner container text-uppercase">
		<div class="banner-content row align-items-center text-center">
			<div class="col-md-4 ml-xl-auto text-md-right">
				<h2 class="mb-md-0 text-white"><?php echo get_sub_field('ads_heading_1'); ?><br><?php echo get_sub_field('ads_heading_2'); ?></h2>
			</div>

			<?php
				$ButtonText = get_sub_field('button_text');
				 if(!empty($ButtonText)): ?>


			<div class="col-md-4 col-xl-3 pb-4 pb-md-0">
				<a href="<?php echo get_sub_field('button_link'); ?>" class="btn btn-dark btn-black ls-10"><?php echo $ButtonText; ?></a>
			</div>

			 <?php endif; ?>

			 <?php
				$PromoTitle = get_sub_field('promo_title');
				 if(!empty($PromoTitle)): ?>


			<div class="col-md-4 mr-xl-auto text-md-left">
				<h4 class="mb-1 coupon-sale-text p-0 d-block ls-10 text-transform-none"><b><?php echo $PromoTitle ?></b></h4>
				<?php
				$PromoSubTitle = get_sub_field('promo_sub_title');
				 if(!empty($PromoSubTitle)): ?>


			<div class="col-md-12 mr-xl-auto text-md-left">
				<h4 class="mb-1 coupon-sale-text p-0 d-block ls-10 text-transform-none"><b><?php echo $PromoSubTitle ?></b></h4>
			</div>

			<?php endif; ?>
			</div>

			<?php endif; ?>

			 



		</div>
	</div>
</section>