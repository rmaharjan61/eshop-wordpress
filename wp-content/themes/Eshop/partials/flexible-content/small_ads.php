<div class="container my-5">
<div class="banner banner-big-sale mb-5" style="background: <?php echo get_sub_field('color'); ?> center/cover url('<?php echo get_sub_field('image'); ?>');">
	<div class="banner-content row align-items-center py-4 mx-0">
		<div class="col-md-9">
			<h2 class="text-white text-uppercase ls-n-20 mb-md-0 px-4">
				<?php
				$PromoTag = get_sub_field('promo_tag');
				 if(!empty($PromoTag)): ?>
                	<b class="d-inline-block mr-4 mb-1 mb-md-0"><?= $PromoTag;  ?></b>
                <?php endif; ?>

				<?php echo get_sub_field('promo_heading'); ?>
				<small class="text-transform-none align-middle"><?php echo get_sub_field('sub_heading'); ?></small>
			</h2>
		</div>
		<?php
				$ButtonText = get_sub_field('button_text');
				 if(!empty($ButtonText)): ?>
                	<div class="col-md-3 text-center text-md-right">
						<a class="btn btn-light btn-white btn-lg mr-3" href="<?php echo get_sub_field('button_link'); ?>"><?= $ButtonText; ?></a>
					</div>
                <?php endif; ?>
		
	</div>
</div>
</div>