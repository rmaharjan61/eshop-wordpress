	<section class="featured-products-section">
				<div class="container">
					<h2 class="section-title heading-border ls-20 border-0"><?= get_sub_field( 'heading' ) ?></h2>
					<div class="row">

<div class="col-6 col-sm-4 col-md-3 col-xl-5col">
	<div class="product-default">
							<?php

						$args = [
							'fields'         => 'ids',
							'post_type'      => 'product',
							'status'         => 'publish',
							'posts_per_page' => get_sub_field( 'products_per_page' ),
							'post__in'       => wc_get_featured_product_ids()
						];

						// var_dump(wc_get_featured_product_ids());

						$featured_products = get_posts( $args );

							foreach ( $featured_products as $product_id ):
								?>
		
			                    
									<?php get_single_product_html( $product_id ); ?>
			                    

							<?php
							endforeach;
							?>
						</div>

						


						 </div>
					</div> <!-- End .Feature Product row -->
				</div>
			</section>