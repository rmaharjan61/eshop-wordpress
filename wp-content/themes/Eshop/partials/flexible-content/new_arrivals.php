<?php
$args = [
	'fields'         => 'ids',
	'post_type'      => 'product',
	'status'         => 'publish',
	'posts_per_page' => get_sub_field( 'products_per_page' ),
	'orderby'        => 'date',
	'order'          => 'DESC'
];

$featured_products = get_posts( $args );
?>

<section class="new-products-section">
				<div class="container">
					<h2 class="section-title heading-border ls-20 border-0 "><?= get_sub_field( 'heading' ) ?></h2>

					<div class="products-slider custom-products owl-carousel owl-theme nav-outer show-nav-hover nav-image-center" data-owl-options="{
						'dots': false,
						'nav': true,
						'responsive': {
							'992': {
								'items': 5
							}
						}
					}">
					
				<?php
                foreach ( $featured_products as $product_id ):
                    ?>
                    
                        <?php get_single_product_html( $product_id ) ?>
                   
                <?php
                endforeach;
                ?>
                	
					</div>
				</div>
			</section>