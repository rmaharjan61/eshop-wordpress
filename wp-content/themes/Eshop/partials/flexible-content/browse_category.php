<section class="new-products-section">
	<div class="container">
<h2 class="section-title heading-border border-0 "><?= get_sub_field( 'heading' ) ?></h2>

<div class="categories-slider owl-carousel owl-theme show-nav-hover nav-outer">

<?php 
					$categories = get_terms( ['taxonomy' => 'product_cat'] );
					// var_dump( $categories );
					foreach ($categories as $cat) {
						if($cat->category_parent == 0) {
				?>

	<div class="product-category">
		<a href="<?php echo get_term_link($cat->slug, 'product_cat') ?> ">
			<figure>
				<img src="<?php echo wp_get_attachment_url( get_term_meta( $cat->term_id, 'thumbnail_id', true )) ?>" style="width:260px; height: 260px; object-fit: cover;">
			</figure>
			<div class="category-content">
				<h3><?php echo $cat->name; ?></h3>
				<span> <b> <?php echo $cat->count; ?> products </b></span>
			</div>
		</a>
	</div>

	 <?php
           }
       } 
                ?>

		</div>

	</div>
</section>



