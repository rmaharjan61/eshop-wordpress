<div class="container">
<!-- 				<div class="info-boxes-slider owl-carousel owl-theme mb-2" data-owl-options="{
					'dots': false,
					'loop': false,
					'responsive': {
						'576': {
							'items': 2
						},
						'992': {
							'items': 3
						}
					}
				}"> -->

<div class="banners-container">
					<div class="banners-slider owl-carousel owl-theme">

						<?php while (have_rows('block')):
		                    the_row();
		                    ?>

						<div class="banner banner1 banner-sm-vw">
							<figure>
								<img src="<?php echo get_sub_field('image'); ?>" alt="banner" style="width: 100%; height: 175px; object-fit: cover;" background-image: linear-gradient(360deg, black, black)>
							</figure>
							<div class="banner-layer banner-layer-middle">
								<h3 class="m-b-2" style="color: white; -webkit-text-stroke-width: 0.5px;  -webkit-text-stroke-color: black;"><?php echo get_sub_field('title'); ?></h3>
								<h4 class="m-b-3 ls-10" style="color: <?php echo get_sub_field('tag_color'); ?>;"><sup class="text-dark"><del><?php echo get_sub_field('stroked_offer_tag'); ?></del></sup><?php echo get_sub_field('offer_tag'); ?></h4>
								<a href="<?php echo get_sub_field('button_link'); ?>" class="btn btn-sm btn-dark ls-10">Shop Now</a>
							</div>
						</div><!-- End .banner -->

					 <?php endwhile; ?>

					</div>
				</div>
				<!-- </div> -->
			</div><!-- End .container -->