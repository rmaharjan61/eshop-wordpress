<section class="blog-section" style="padding-top: 0px !important;">
				<div class="container">
					<!-- <hr class="mt-4 m-b-5"> -->
					<div class="brands-slider owl-carousel owl-theme images-center pb-2">
						<?php 
					          	$images = get_sub_field('brands');
					         		foreach ($images as $img) {
									?>      
									<img src="<?php echo $img; ?>" style="width: 140px; height: 60px; object-fit: cover;"  alt="brand">
		           				 <?php  
		           					}  
		            			?>
		            </div><!-- End .brands-slider -->
				</div>
			</section>