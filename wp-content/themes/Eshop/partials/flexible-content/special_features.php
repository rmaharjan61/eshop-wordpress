<div class="container">
				<div class="info-boxes-slider owl-carousel owl-theme mb-2" data-owl-options="{
					'dots': false,
					'loop': false,
					'responsive': {
						'576': {
							'items': 2
						},
						'992': {
							'items': 3
						}
					}
				}">
						<?php while (have_rows('block')):
		                    the_row();
		                    ?>

					<div class="info-box info-box-icon-left">
						<?php echo get_sub_field('icon'); ?>
						<div class="info-box-content">
							<h4><?php echo get_sub_field('title'); ?></h4>
							<p class="text-body"><?php echo get_sub_field('sub-title'); ?></p>
						</div><!-- End .info-box-content -->
					</div><!-- End .info-box -->
					
						 <?php endwhile; ?>

				</div><!-- End .info-boxes-slider -->				
			</div><!-- End .container -->