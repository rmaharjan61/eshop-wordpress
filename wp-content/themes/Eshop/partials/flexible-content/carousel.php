<div class="home-slider owl-carousel owl-theme owl-carousel-lazy show-nav-hover nav-big mb-2 text-uppercase" data-owl-options="{
        'loop': true
      }">

             <?php while (have_rows('block')):
                    the_row();

                    $Image = get_sub_field('image');
                    $TopTitle = get_sub_field('title');
                    $FancyHeading = get_sub_field('fancy_heading');
                    $PromoHeading = get_sub_field('promo_heading');
                    $OfferHeading = get_sub_field('offer_heading');
                    $OfferText = get_sub_field('offer_text');
                    $ButtonText=get_sub_field('button_text');
                    $ButtonLink=get_sub_field('button_link');
                    ?>


        <div class="home-slide home-slide1 banner">
          <img class="owl-lazy slide-bg" src="<?= $Image;?>" data-src="<?= $Image;?>" alt="slider image" style=" width: 100%;  height: 518px; object-fit: cover;">
          <div class="container">
            <div class="banner-layer banner-layer-middle">
              <h4 class="text-transform-none m-b-3"><?= $TopTitle;?></h4>
              <h2 class="text-transform-none mb-0"><?= $FancyHeading;?></h2>
              <h3 class="m-b-3"><?= $PromoHeading;?></h3>
              <h5 class="d-inline-block mb-0">
                <?= $OfferHeading;?>
                <?php if(!empty($OfferText)): ?>
                <b class="coupon-sale-text ml-4 mr-1 text-white bg-secondary align-middle"><em class="align-text-top"><?= $OfferText;?></em></b>
                <?php endif; ?>
              </h5>
              <a href="<?= $ButtonLink;?>" class="btn btn-dark btn-lg ls-10"><?= $ButtonText;?></a>
            </div><!-- End .banner-layer -->
          </div>
        </div><!-- End .home-slide -->

 <?php endwhile; ?>

      </div><!-- End .home-slider -->