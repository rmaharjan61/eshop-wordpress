<?php

/**
 * @param $name
 * @param array $args
 * https://developer.wordpress.org/resource/dashicons/#schedule
 */
function add_post_type( $name, $args = array() ) {
	add_action( 'init', function () use ( $name, $args ) {
		$upper = ucwords( $name );
		$name  = strtolower( str_replace( ' ', '_', $name ) );

		$args = array_merge(
			[
				'public'      => true,
				'label'       => $upper . "s",
				'labels'      => array( 'add_new_item' => "Add new $upper" ),
				'supports'    => array( 'title', 'editor', 'comments', 'thumbnail' ),
				'has_archive' => true
			],
			$args );

		register_post_type( $name, $args );
	} );
}

function add_taxonomy( $name, $post_type, $args = array() ) {
	$name = strtolower( $name );
	add_action( 'init', function () use ( $name, $post_type, $args ) {
		$args = array_merge(
			[
				'label' => ucwords( $name )
			],
			$args );

		register_taxonomy( $name, $post_type, $args );
	} );
}


add_post_type( 'Courses', [
	'public'      => true,
	//    'menu_position' => 5,
	'label'       => 'Courses',
	'menu_icon'   => 'dashicons-book',
	'labels'      => [ 'add_new_item' => "Add New Course" ],
	'taxonomies'  => [ 'courses-category','Subject' ],
	'supports'    => [ 'title', 'editor', 'thumbnail' ],
	'has_archive' => true
] );

$labels = array(
    'name'                       => _x( 'Courses Categories', 'taxonomy general name' ),
    'singular_name'              => _x( 'Courses Category', 'taxonomy singular name' ),
    'search_items'               => __( 'Search Courses Categories' ),
    'popular_items'              => __( 'Popular Courses Categories' ),
    'all_items'                  => __( 'All Courses Categories' ),
    'parent_item'                => null,
    'parent_item_colon'          => null,
    'edit_item'                  => __( 'Edit Courses Category' ),
    'update_item'                => __( 'Update Courses Category' ),
    'add_new_item'               => __( 'Add New Courses Category' ),
    'new_item_name'              => __( 'New Courses Category Name' ),
    'separate_items_with_commas' => __( 'Separate Courses categories with commas' ),
    'add_or_remove_items'        => __( 'Add or remove Courses categories' ),
    'choose_from_most_used'      => __( 'Choose from the most used Courses categories' ),
    'menu_name'                  => __( ' Courses Categories' ),
);

add_taxonomy( "courses-category", 'courses', [
	'labels'       => $labels,
    'hierarchical' => false
] );

$labels = array(
    'name'                       => _x( 'Subjects', 'taxonomy general name' ),
    'singular_name'              => _x( 'Subject', 'taxonomy singular name' ),
    'search_items'               => __( 'Search Subjects' ),
    'popular_items'              => __( 'Popular Subjects' ),
    'all_items'                  => __( 'All Subjects' ),
    'parent_item'                => null,
    'parent_item_colon'          => null,
    'edit_item'                  => __( 'Edit Subject' ),
    'update_item'                => __( 'Update Subject' ),
    'add_new_item'               => __( 'Add New Subject' ),
    'new_item_name'              => __( 'New Subject Name' ),
    'separate_items_with_commas' => __( 'Separate topics with commas' ),
    'add_or_remove_items'        => __( 'Add or remove topics' ),
    'choose_from_most_used'      => __( 'Choose from the most used Subjects' ),
    'menu_name'                  => __( 'Subjects' ),
);
add_taxonomy( 'subject', 'courses', [
    'labels'       => $labels,
    'hierarchical' => true
] );





flush_rewrite_rules();


