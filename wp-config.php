<?php
define( 'WP_CACHE', true );
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'eshop' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'Z%A8V+H &l=&mZ4;m1~/?RjrA<$xA]qt!LTlpiaXs7oXl5xSX%,Tne@Z<y^`9)mr' );
define( 'SECURE_AUTH_KEY',  '`mBBtgrQ4=aQt_0u|uI~7b7g)dU*Gn7sgrb%[CV#tep+k~_>=ez t5}zQ+94`!- ' );
define( 'LOGGED_IN_KEY',    'p#}iQ:(IcX^uPVO2>l~,p4}_&r&=GP>ySG+]<vN/Is^gS|mc-ST(*fRiz~]ZVM~.' );
define( 'NONCE_KEY',        'TmFh8Kj+/p]^B}Z>Q/rJ MLd9HeuhC!1>IUUyU(>]>.M/MchqnME ppz9Z4yiQd$' );
define( 'AUTH_SALT',        '_) XS*7,ymYTO=!8X_{%(n.f.SnnngPWI^pc@mUZ^|(3/bkd? wdMp1!zAPsYjif' );
define( 'SECURE_AUTH_SALT', '=V~Mx9{3<qAZ$y~=gZBH@R|Q_9d/XM/33t+H-#fN#sIUKc0|6mAx?-8x/NjE}6mz' );
define( 'LOGGED_IN_SALT',   '3J#!:)ITba&we.8UWDK^lPB!o.-&u~*ETfv8;~I5s;c=C3-l[>3 S3yEVBiOp|Y ' );
define( 'NONCE_SALT',       '.pta};s$[3rmi^[RqY 3Z#4*W8>T#)KN0QHI}m`~=g}UJW>i}*>1wKk4p&VXl+!n' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
